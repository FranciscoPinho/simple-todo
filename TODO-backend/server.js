require("dotenv").config()
const projectResource = require("./api/resources/ProjectResource.js")
const usersResource = require("./api/resources/UsersResource.js")
const cors = require("cors")
const cookieParser = require("cookie-parser")
const bodyParser = require("body-parser")
const express = require("express"),
  app = express(),
  port = process.env.PORT || 3001

app.use(cors())
app.use(bodyParser.json())
app.use(cookieParser())
app.use(
  bodyParser.urlencoded({
    extended: true
  })
)
app.use("/projects/",  projectResource)
app.use("/users/",  usersResource)
app.listen(port)

module.exports = app
