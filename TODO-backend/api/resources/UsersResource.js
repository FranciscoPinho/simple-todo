const db = require("../fakeDb/inMemoryDb")
const mw = require("../middleware/validationMiddleware")
const auth = require("../middleware/authenticationMiddleware")
const express = require("express")

const router = express.Router()

router.post("/", mw.objectValidationMiddleware("user"), (req, res) => {
  try {
    const token = db.addUser(req.body.user)
    res.status(201).send({token})
  } catch (err) {
    console.log(err)
    res.status(400).send(err)
  }
})

router.post("/login", mw.objectValidationMiddleware("user"), (req, res) => {
  try {
    const token = db.loginUser(req.body.user)

    if (token)  return res.status(202).send({token})

    res.status(401).end()
  } catch (err) {
    console.log(err)
    res.status(401).send(err)
  }
})

router.post("/login/token", auth.authMiddleware(), (req, res) => {
  try {
    res.status(202).send({ username: req.meta.username})
  } catch (err) {
    console.log(err)
    res.status(401).send(err)
  }
})


module.exports = router
