const db = require("../fakeDb/inMemoryDb")
const mw = require("../middleware/validationMiddleware")
const auth = require("../middleware/authenticationMiddleware")
const express = require("express")

const router = express.Router()

router.get("/", auth.authMiddleware(), (req, res) => {
  try {
    const user = db.getUser(req.meta.username)
    res.status(200).send({ projects: user.projects })
  } catch (err) {
    res.status(400).send(err)
  }
})

router.post("/", auth.authMiddleware(), mw.fieldsValidationMiddleware("project"), (req, res) => {
  try {
    db.addProject(req.meta.username, req.body.project)
    res.status(201).end()
  } catch (err) {
    res.status(400).send(err)
  }
})

router.post(
  "/tasks",
  auth.authMiddleware(),
  mw.objectValidationMiddleware("task"),
  mw.fieldsValidationMiddleware("project"),
  (req, res) => {
    try {
      const project = db.getProject(req.meta.username, req.body.project)
      db.addTask(project, req.body.task)
      res.status(201).end()
    } catch (err) {
      res.status(400).send(err)
    }
  }
)

router.put(":project/tasks/:taskId", auth.authMiddleware(), (req, res) => {
  try {
    const project = db.getProject(req.meta.username, req.params.project)
    db.finishTask(project, req.params.taskId)
    res.status(200).end()
  } catch (err) {
    res.status(400).send(err)
  }
})

module.exports = router
