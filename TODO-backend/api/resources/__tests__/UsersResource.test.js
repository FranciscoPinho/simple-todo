const app = require("../../../server")
const request = require("supertest")
const testUsername = "testUser"
const testPassword = "123adawdfefs"

describe("user resource endpoints", () => {
  it("should register new user", async () => {
    const res = await request(app)
      .post("/users/")
      .send({
        user: {
          username: testUsername,
          password: testPassword
        }
      })
    expect(res.statusCode).toEqual(201)
  })
  it("should not register new user with invalid password", async () => {
    const res = await request(app)
      .post("/users/")
      .send({
        user: {
          password: testPassword
        }
      })
    expect(res.statusCode).toEqual(400)
  })
  it("should not register new user with invalid username", async () => {
    const res = await request(app)
      .post("/users/")
      .send({
        user: {
          username: testUsername,
        }
      })
    expect(res.statusCode).toEqual(400)
  })
  it("should login existing user", async () => {
    const res = await request(app)
      .post("/users/login")
      .send({
        user: {
          username: testUsername,
          password: testPassword
        }
      })
    expect(res.statusCode).toEqual(202)
  })
})
