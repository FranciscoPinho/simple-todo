const jwt = require("jsonwebtoken")

exports.authMiddleware = () => (req, res, next) => {
  try {
    const decoded = jwt.verify(req.get("authorization"), process.env.JWT_SECRET)

    if (!decoded.username) throw new Error("Invalid token")
    req.meta = {}
    req.meta.username = decoded.username
    next()
  } catch (err) {
    next(err)
  }
}
