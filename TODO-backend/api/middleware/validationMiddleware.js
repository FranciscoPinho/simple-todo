const Joi = require("@hapi/joi")

exports.objectValidationMiddleware = objectType => (req, res, next) => {
  const validation = objValidations[objectType].validate(req.body[objectType])
  if (validation.error) return res.status(400).send(validation.error)
  next()
}

exports.fieldsValidationMiddleware = (...fields) => (req, res, next) => {
  fields.forEach(field => {
    const validation = fieldValidations[field].validate(req.body[field])
    if (validation.error) return res.status(400).send(validation.error)
  })

  next()
}

const fieldValidations = {
  username: Joi.string()
    .alphanum()
    .min(3)
    .max(30)
    .required(),
  project: Joi.string().required(),
  password: Joi.string()
    .pattern(/.{8,30}$/)
    .required()
}

const objValidations = {
  user: Joi.object({
    username: fieldValidations.username,
    password: fieldValidations.password
  }),
  task: Joi.object({
    project: fieldValidations.username,
    description: Joi.string().required(),
    creationDate: Joi.date().required(),
    finishDate: Joi.date().required()
  })
}
