const jwt = require("jsonwebtoken")
const bcrypt = require("bcryptjs")

exports.users = {}

const TASK_STATES = {
  DONE: "DONE",
  TODO: "TODO"
}

const uuidv4 = () => {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (c ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))).toString(16)
  )
}

exports.addUser = ({username, password}) => {
  if (username in this.users) throw new Error("user already registered")

  const salt = bcrypt.genSaltSync(10)
  const hashedPassword = bcrypt.hashSync(password, salt)
  const token = jwt.sign({ username }, process.env.JWT_SECRET)

  this.users[username] = {
    password: hashedPassword,
    token,
    projects: {}
  }

  return token
}

exports.getUser = username => {
  if (!(username in this.users)) throw new Error(`${username} does not exist`)

  return this.users[username]
}

exports.loginUser = ({username, password}) => {
  try {
    const user = this.getUser(username)
    if (!bcrypt.compareSync(password, user.password)) return false

    user.token = jwt.sign({ username }, process.env.JWT_SECRET)

    return user.token
  } catch (err) {
    throw err
  }
}

exports.authenticateUser = token => {
  try {
    return this.getUser(username).token === token
  } catch (err) {
    throw err
  }
}

exports.addProject = (username, projectname) => {
  try {
    const user = this.getUser(username)
    user.projects[projectname] = { tasks: [] }
  } catch (err) {
    throw err
  }
}

exports.getProject = (username, projectname) => {
  if (!(username in this.users)) throw new Error(`${username} does not exist`)
  const projects = this.users[username].projects
  if(!(projectname in projects)) throw new Error(`${projectname} does not exist`)

  return projects[projectname]
}

exports.addTask = (project, task) => {
  try {
    project.tasks.push({
      id: uuidv4(),
      ...task,
      state: TASK_STATES.TODO
    })
  } catch (err) {
    throw err
  }
}

exports.getTaskById = (tasks, taskId) => {
  for (let i = 0, len = tasks.length; i < len; i++) {
    if (tasks[i].id === taskId) return tasks[i]
  }

  throw new Error("task not found")
}

exports.finishTask = (project, taskId) => {
  try {
    this.getTaskById(project.tasks, taskId).state = TASK_STATES.DONE
  } catch (err) {
    throw err
  }
}
