const db = require("../inMemoryDb")

const testUsername = "testUser"
const testPassword = "123adawdfefs"
process.env.JWT_SECRET='secret'

describe("user services test", () => {
  it("should test addition", () => {
    db.addUser({username: testUsername, password: testPassword})
    expect(db.users[testUsername]).toBeTruthy()
  })
  it("should test user already exists", () => {
    expect(()=>db.addUser(testUsername, "123")).toThrow(Error);
  })
  it("should fail when getting non existent user", () => {
    expect(()=>db.getUser("sadad")).toThrow(Error);
  })
  it("should get user", () => {
    expect(db.getUser(testUsername)).toBeTruthy()
  })
})



  