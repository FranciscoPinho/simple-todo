import React from "react"
import { Segment } from "semantic-ui-react"

const Project = props => <Segment>{props.name}</Segment>

export default Project
