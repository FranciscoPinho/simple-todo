import React, { useState } from "react"
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom"
import MyProjects from "../pages/MyProjects"
import LoginPage from "../pages/LoginPage"

const Router = props => {
  const [loggedUser, setLoggedUser] = useState(null)
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <LoginPage login={username => setLoggedUser(username)} />
        </Route>
        <PrivateRoute path="/projects" isAuthenticated={!!loggedUser}>
          <MyProjects
            user={loggedUser}
            logout={() => {
              delete window.sessionStorage.token
              setLoggedUser(null)
            }}
          />
        </PrivateRoute>
      </Switch>
    </BrowserRouter>
  )
}

function PrivateRoute(props) {
  return (
    <Route
      render={({ location }) =>
        props.isAuthenticated ? (
          props.children
        ) : (
          <Redirect
            to={{
              pathname: "/"
            }}
          />
        )
      }
    />
  )
}

export default Router
