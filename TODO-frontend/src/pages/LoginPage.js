import React, { useState, useEffect } from "react"
import { Container, Button, Form } from "semantic-ui-react"
import axios from "axios"
import { useHistory } from "react-router-dom"
import { server_url } from "../index"

const handleLogin = async (username, password, login, history) => {
  try {
    const response = await axios.post(`${server_url}/users/login`, {
      user: { username, password }
    })
    window.sessionStorage.token = response.data.token
    login(username)
    history.replace("/projects")
  } catch (err) {
    console.log(err)
  }
}

const handleRegister = async (username, password, login, history) => {
  try {
    const response = await axios.post(`${server_url}/users/`, {
      user: { username, password }
    })
    window.sessionStorage.token = response.data.token
    login(username)
    history.replace("/projects")
  } catch (err) {
    console.log(err)
  }
}

const LoginPage = props => {
  const history = useHistory()
  const [isLoggingIn, setisLoggingIn] = useState(false)
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const { login } = props
  useEffect(() => {
    const tryLoginFromToken = async () => {
      try {
        const response = await axios.post(`${server_url}/users/login/token`, {}, { headers: { authorization: window.sessionStorage.token } })
        login(response.data.username)
        history.replace("/projects")
      } catch (err) {
        console.log(err)
      }
    }
    tryLoginFromToken()
  }, [login, history])

  return (
    <Container>
      <Button
        content="Login"
        style={{ opacity: isLoggingIn ? 1 : 0.4 }}
        active={!isLoggingIn}
        primary
        onClick={() => setisLoggingIn(true)}
      />
      <Button
        content="Register"
        style={{ opacity: isLoggingIn ? 0.4 : 1 }}
        active={!!isLoggingIn}
        primary
        onClick={() => setisLoggingIn(false)}
      />
      <Form
        onSubmit={
          isLoggingIn
            ? () => handleLogin(username, password, props.login, history)
            : () => handleRegister(username, password, props.login, history)
        }
      >
        <Form.Input
          placeholder="Username"
          name="username"
          value={username}
          onChange={(e, { name, value }) => setUsername(value)}
        />
        <Form.Input
          placeholder="password"
          name="password"
          value={password}
          onChange={(e, { name, value }) => setPassword(value)}
        />
        <Button type="submit">Submit</Button>
      </Form>
    </Container>
  )
}

export default LoginPage
