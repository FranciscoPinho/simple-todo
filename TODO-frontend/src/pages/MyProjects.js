import React, { useState, useEffect } from "react"
import { Container, Button, Form } from "semantic-ui-react"
import axios from "axios"
import Project from "../components/Project"
import { server_url } from "../index"

const MyProjects = ({ logout, user }) => {
  const [projects, setProjects] = useState({})
  const [newProject, setNewProject] = useState("")

  const addProject = async () => {
    try {
      await axios.post(
        `${server_url}/projects/`,
        {
          project: newProject
        },
        { headers: { authorization: window.sessionStorage.token } }
      )
      setProjects({ [newProject]: { tasks: [] }, ...projects })
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => {
    const fetchPlans = async () => {
      try {
        const res = await axios.get(`${server_url}/projects/`, {
          headers: { authorization: window.sessionStorage.token }
        })
        setProjects(res.data.projects)
      } catch (err) {
        console.log(err)
      }
    }
    fetchPlans()
  }, [user, setProjects])

  return (
    <Container>
      <Button content="Logout" primary onClick={() => logout()} />
      <Form onSubmit={addProject}>
        <Form.Input
          placeholder="Project name"
          name="project"
          value={newProject}
          onChange={(e, { name, value }) => setNewProject(value)}
        />
        <Button type="submit" onSubmit={addProject}>Submit</Button>
      </Form>
      {Object.keys(projects).map((proj, index) => (
        <Project name={proj} key={index} />
      ))}
    </Container>
  )
}

export default MyProjects
