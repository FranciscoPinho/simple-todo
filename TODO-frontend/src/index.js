import React from "react"
import ReactDOM from "react-dom"
import "./index.css"
import Router from "./router/router"

export var server_url = process.env.REACT_APP_SERVER_BASE_URL
ReactDOM.render(<Router />, document.getElementById("root"))
